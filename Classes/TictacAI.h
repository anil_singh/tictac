/*
 * TictacAI.h
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#ifndef TICTACAI_H_
#define TICTACAI_H_

#include "Board.h"

class TictacAI {
public:
	TictacAI();
	virtual ~TictacAI();
	static bool alternateMove(Board* gameBoard, vector<Vec2> moves);
	static bool winMove(Board* gameBoard, Charater_Type char_type,
			vector<Vec2> moves);
	static bool AllCheck(Board* gameBoard, Charater_Type type,
	vector<Vec2> moves);
	static bool checkType_1(Board* gameBoard, Charater_Type type,
			vector<Vec2> moves, Vec2 pos, bool isx_increment);
	static bool checkType_2(Board* gameBoard, Charater_Type type,
			vector<Vec2> moves, Vec2 pos, bool alternate);
private:
	static void changeTurn(LocationNode * temp);
};

#endif /* TICTACAI_H_ */
