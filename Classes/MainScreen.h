/*
 * MainScreen.h
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#ifndef MAINSCREEN_H_
#define MAINSCREEN_H_
#include "cocos2d.h"

USING_NS_CC;
class MainScreen : public Layer {
public:
	MainScreen();
	virtual ~MainScreen();
	bool init();
	static Scene* createScene();
	CREATE_FUNC(MainScreen);

private:
	Size size;
	void onButtonClick(Ref* sender);
	void onClick();
	Sprite *music, *sound;
	Sprite *aboutUs, *moreApps;

};

#endif /* MAINSCREEN_H_ */
