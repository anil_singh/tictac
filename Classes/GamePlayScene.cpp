#include "GamePlayScene.h"
#include "TictacAI.h"
#include "GameOver.h"
#include "MainScreen.h"
#include "SonarFrameworks.h"

using namespace SonarCocosHelper;


USING_NS_CC;

Scene* GamePlayScene::createScene() {
	auto scene = Scene::create();
	auto layer = GamePlayScene::create();
	scene->addChild(layer);
	return scene;
}

bool GamePlayScene::init() {
	if (!Layer::init()) {
		return false;
	}
SonarCocosHelper::GoogleAnalytics::setScreenName("HelloWorld Screen");

	GameUtils* gameUtils = GameUtils::getGameUtilsInstance();
	if (gameUtils->isMusicOn()) {
		//log("In hello world init");
		gameUtils->playMusic();
	}
	game_over = false;
	size = Director::getInstance()->getVisibleSize();

	auto home = MenuItemImage::create("home.jpg", "home.jpg",
			CC_CALLBACK_1(GamePlayScene::menuCloseCallback, this));

	home->setPosition(
			Vec2(size.width - home->getContentSize().width / 2,
					home->getContentSize().height / 2));

	auto menu = Menu::create(home, NULL);
	menu->setPosition(Vec2::ZERO);
//	this->addChild(menu, 1);

	label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 45);
	label->setPosition(
			Vec2(size.width / 2,
					size.height / 4 - label->getContentSize().height));

	this->addChild(label, 1);
	auto sprite = Sprite::create("bg3.jpg");
	sprite->setPosition(Vec2(size.width / 2, size.height / 2));
	addChild(sprite, -1);
	// add the sprite as a child to this layer
	gameUtils->setFirstTurn(Turn::PlayerA);
	gameUtils->setCurrentTurn(Turn::PlayerA);
	label->setString(
			gameUtils->getPlayerName(gameUtils->getCurrentTurn()) + " Turn");
	gameBoard = new Board();
	gameBoard->setPosition(size.width * 0.07, size.height * .28);
	addChild(gameBoard);

	SonarCocosHelper::AdMob::showBannerAd(AdBannerPosition::eBottom);
	return true;
}

void GamePlayScene::play(LocationNode *pos_sp) {
	GameUtils *gameUtils = GameUtils::getGameUtilsInstance();
	bool result = true;
	if (gameUtils->getCurrentTurn() == Turn::PlayerA) {
		//log("In player turn A");

//		gameUtils->vibrateIt(100L);
		if (gameUtils->isEffectOn()) {
			gameUtils->playEffect();
		}

		pos_sp->setTexture(
				Director::getInstance()->getTextureCache()->addImage(
						"cross.png"));
		gameUtils->setCurrentTurn(Turn::PlayerB);
		label->setString(
				gameUtils->getPlayerName(gameUtils->getCurrentTurn())
						+ " Turn");
		gameUtils->setPlayerA_Moves(1);
		pos_sp->setOcupied(true);
		pos_sp->set_Type(Charater_Type::Cross);
		gameUtils->setListPA_MovesCoor(pos_sp->getnod_Cordinate());
		result = checkForWinner(gameUtils->getListPA_MovesCoor());

//		gameUtils->stopVibration();

		if (result) {
//			AdMob::showPreLoadedFullscreenAd();
			gameUtils->setCurrentTurn(Turn::None);
			GameOver::name = "Player A";
			game_over = true;
			gameOver();
		} else if (gameUtils->getCurrentTurn() == Turn::None) {
			game_over = true;
			gameOver();
		}

	} else if (gameUtils->getGameType() == Game_Type::Double
			&& gameUtils->getCurrentTurn() == Turn::PlayerB) {
		//log("In player turn B");
//		gameUtils->vibrateIt(100L);
		if (gameUtils->isEffectOn()) {
			gameUtils->playEffect();
		}
		pos_sp->setTexture(
				Director::getInstance()->getTextureCache()->addImage(
						"zero.png"));
		gameUtils->setCurrentTurn(Turn::PlayerA);
		label->setString(
				gameUtils->getPlayerName(gameUtils->getCurrentTurn())
						+ " Turn");
		gameUtils->setPlayerB_Moves(1);
		pos_sp->setOcupied(true);
		pos_sp->set_Type(Charater_Type::Zero);
		gameUtils->setListPB_MovesCoor(pos_sp->getnod_Cordinate());
		result = checkForWinner(gameUtils->getListPB_MovesCoor());

//		gameUtils->stopVibration();

		if (result) {
//			AdMob::showPreLoadedFullscreenAd();
			gameUtils->setCurrentTurn(Turn::None);
			GameOver::name = "Player B";
			game_over = true;
			gameOver();
		} else if (gameUtils->getCurrentTurn() == Turn::None) {
			game_over = true;
			gameOver();
		}
	}
	if (!result && gameUtils->getGameType() == Game_Type::Single
			&& gameUtils->getCurrentTurn() != Turn::None) {
		//log("In player turn computer");
		scheduleOnce(SEL_SCHEDULE(&GamePlayScene::computerMove), .5f);
	}
}

void GamePlayScene::computerMove(float dt) {
	bool result = false;
	GameUtils *gameUtils = GameUtils::getGameUtilsInstance();
	if (gameUtils->getGameType() == Game_Type::Single) {
//		gameUtils->vibrateIt(100L);
		if (gameUtils->isEffectOn()) {
			gameUtils->playEffect();
		}
		if (TictacAI::AllCheck(gameBoard, Charater_Type::Zero,
				gameUtils->getListPB_MovesCoor())) {
			result = checkForWinner(gameUtils->getListPB_MovesCoor());
		} else if (TictacAI::AllCheck(gameBoard, Charater_Type::Cross,
				gameUtils->getListPA_MovesCoor())) {
			result = checkForWinner(gameUtils->getListPB_MovesCoor());
		} else if (TictacAI::alternateMove(gameBoard,
				gameUtils->getListPA_MovesCoor())) {
			result = checkForWinner(gameUtils->getListPB_MovesCoor());
		}
	}

//	gameUtils->stopVibration();

	label->setString(
			gameUtils->getPlayerName(gameUtils->getCurrentTurn()) + " Turn");
	if (result) {
		GameUtils::getGameUtilsInstance()->setCurrentTurn(Turn::None);
		GameOver::name = "Computer";
		game_over = true;
		gameOver();
	} else if (gameUtils->getCurrentTurn() == Turn::None) {
		game_over = true;
		gameOver();
	}

}
void GamePlayScene::menuCloseCallback(Ref* pSender) {
	GameUtils::getGameUtilsInstance()->re_initialize();
	Director::getInstance()->replaceScene(MainScreen::createScene());
}

void GamePlayScene::gameOver() {
	//log("Game over a");
	if (game_over) {
		//log("Game over");
		if (label->isVisible())
			label->setVisible(false);
		GameOver * layer = GameOver::create();
		this->addChild(layer);
		game_over = false;
	}
}

bool GamePlayScene::checkForWinner(vector<Vec2> moves) {
	bool result = false;
	if (checkType_1(moves, Vec2(0, 0), true)) {
		//log("Winner In the 0 0 vertical");
		result = true;
	} else if (checkType_1(moves, Vec2(0, 1), true)) {
		//log("Winner In the 0 1 vertical");
		result = true;
	} else if (checkType_1(moves, Vec2(0, 2), true)) {
		//log("Winner In the 0 2 vertical");
		result = true;
	} else if (checkType_2(moves, Vec2(0, 0), false)) {
		//log("Winner In the 0 0 diagonal");
		result = true;
	} else if (checkType_2(moves, Vec2(2, 0), true)) {
		//log("Winner In the 2 0 diagonal");
		result = true;
	} else if (checkType_1(moves, Vec2(0, 0), false)) {
		//log("Winner In the 0 0 horizontal");
		result = true;
	} else if (checkType_1(moves, Vec2(1, 0), false)) {
		//log("Winner In the 1 0 horizontal");
		result = true;
	} else if (checkType_1(moves, Vec2(2, 0), false)) {
		//log("Winner In the 2 0 horizontal");
		result = true;
	}

	if (!result && GameUtils::getGameUtilsInstance()->getTotalMoves() > 8) {
		GameOver::name = "No one";
		GameUtils::getGameUtilsInstance()->setCurrentTurn(Turn::None);
		//gameOver();
	}
	return result;
}

bool GamePlayScene::checkType_2(vector<Vec2> moves, Vec2 pos, bool alternate) {
	bool result = false;
	int count = 0;
	int success_count = 0;
	Vec2 tempPos;

	//log("In the Check2 posx = %f   posy = %f", pos.x, pos.y);

	while (alternate ?
			((pos.x - count) > -1 && (pos.y + count) < 3) :
			((pos.x + count) < 3 && (pos.y + count) < 3)) {
		tempPos =
				alternate ?
						Vec2((pos.x - count), (pos.y + count)) :
						Vec2((pos.x + count), (pos.y + count));

//		log("Pos-x = %f  Pos-y = %f", tempPos.x, tempPos.y);

		if (GameUtils::getGameUtilsInstance()->customFind(moves, tempPos)) {
			success_count++;
			count++;
		} else {
			count++;
		}
	}

	if (success_count > 2) {
		gameBoard->drawCrossLine(pos, tempPos);
		result = true;
	}
	return result;
}

bool GamePlayScene::checkType_1(vector<Vec2> moves, Vec2 pos, bool isx_increment) {
	bool result = false;
	int count = 0;
	int success_count = 0;
	Vec2 tempPos;
	int temp = isx_increment ? pos.x : pos.y;

	//log("In the Check1 posx = %f   posy = %f", pos.x, pos.y);

	while ((temp + count) < 3) {
		tempPos =
				isx_increment ?
						Vec2((pos.x + count), pos.y) :
						Vec2(pos.x, (pos.y + count));

//		log("Pos-x = %f  Pos-y = %f", tempPos.x, tempPos.y);

		if (GameUtils::getGameUtilsInstance()->customFind(moves, tempPos)) {
			success_count++;
			count++;
		} else {
			break;
		}
	}

	if (success_count > 2) {
		gameBoard->drawCrossLine(pos, tempPos);
		result = true;
	}
	return result;
}

void GamePlayScene::onExit() {
	Layer::onExit();
//	GameUtils::getGameUtilsInstance()->stopMusic();
}

