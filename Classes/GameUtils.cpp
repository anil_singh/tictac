/*
 * GameUtils.cpp
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#include "GameUtils.h"
#include <jni.h>
#include <platform/android/jni/JniHelper.h>
#include "SimpleAudioEngine.h"

GameUtils* GameUtils::gameUtilsInstance = nullptr;
const char* SOUND_EFFECT = "SOUND_EFFECT";
const char* BACKGROUND_MUSIC = "BACKGROUND_MUSIC";

GameUtils::GameUtils() {
	// TODO Auto-generated constructor stub
	re_initialize();
}

void GameUtils::re_initialize() {
	move_count = 0;
	playerA_count = playerB_count = 0; //0, 2, 6, 8 //1, 3, 5, 7
	cornerMoves.clear();
	sideMoves.clear();
	cornerMoves.push_back(0);
	cornerMoves.push_back(2);
	cornerMoves.push_back(6);
	cornerMoves.push_back(8);
	sideMoves.push_back(1);
	sideMoves.push_back(3);
	sideMoves.push_back(5);
	sideMoves.push_back(7);
	playerA_Moves.clear();
	playerB_Moves.clear();
}

GameUtils::~GameUtils() {
	// TODO Auto-generated destructor stub
}

GameUtils* GameUtils::getGameUtilsInstance() {
	if (gameUtilsInstance == nullptr) {
		gameUtilsInstance = new GameUtils();
	}
	return gameUtilsInstance;
}

Turn GameUtils::getCurrentTurn() {
	return turn;
}

void GameUtils::setCurrentTurn(Turn turn) {
	this->turn = turn;
}

Game_Type GameUtils::getGameType() {
	return game_type;
}

void GameUtils::setGameType(Game_Type game_type) {
	this->game_type = game_type;
}

void GameUtils::setPlayerA_Moves(int count) {
	playerA_count += count;
}

void GameUtils::setPlayerB_Moves(int count) {
	playerB_count += count;
}

int GameUtils::getPlayerA_Moves() {
	return playerA_count;
}

int GameUtils::getPlayerB_Moves() {
	return playerB_count;
}

int GameUtils::getTotalMoves() {
	return playerA_count + playerB_count;
}

void GameUtils::setFirstTurn(Turn fst_turn) {
	first_turn = fst_turn;
}

Turn GameUtils::getFirstTurn() {
	return first_turn;
}

void GameUtils::setListPA_MovesCoor(Vec2 pos) {
	playerA_Moves.push_back(pos);
}

void GameUtils::setListPB_MovesCoor(Vec2 pos) {
	playerB_Moves.push_back(pos);
}

vector<Vec2> GameUtils::getListPA_MovesCoor() {
	return playerA_Moves;
}

vector<Vec2> GameUtils::getListPB_MovesCoor() {
	return playerB_Moves;
}

vector<int> GameUtils::getCornerMoves() {
	return cornerMoves;
}

vector<int> GameUtils::getSideMoves() {
	return sideMoves;
}

bool GameUtils::customFind(vector<Vec2> moves, Vec2 toFind) {
	for (int var = 0; var < moves.size(); ++var) {
		if (moves.at(var) == toFind)
			return true;
	}
	return false;
}

string GameUtils::getPlayerName(Turn type) {
	string str = "";
	switch (type) {
	case Turn::PlayerA:
		str = "Player A";
		break;
	case Turn::PlayerB:
		str = "Player B";
		break;
	}
	return str;
}

void GameUtils::vibrateIt(long time) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "vibrateIt", "(J)V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, time);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void GameUtils::stopVibration() {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "stopVibration", "()V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void GameUtils::setEffect(bool effect) {
	UserDefault::getInstance()->setBoolForKey(SOUND_EFFECT, effect);
}

void GameUtils::setMusic(bool music) {
	UserDefault::getInstance()->setBoolForKey(BACKGROUND_MUSIC, music);
}

bool GameUtils::isEffectOn() {
	return UserDefault::getInstance()->getBoolForKey(SOUND_EFFECT, true);
}

bool GameUtils::isMusicOn() {
	return UserDefault::getInstance()->getBoolForKey(BACKGROUND_MUSIC, true);
}

void GameUtils::playEffect() {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(
			"effect.mp3", false);
}

void GameUtils::playMusic() {
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(
			"music_1.mp3", true);
}

void GameUtils::stopMusic() {
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
}

void GameUtils::openUrl(string url, string altrUrl) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openUrl",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(url.c_str());
		jstring stringArg2 = methodInfo.env->NewStringUTF(altrUrl.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1, stringArg2);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
		methodInfo.env->DeleteLocalRef(stringArg2);
	}
}

void GameUtils::sendFeedback(string reciever_emailid, string subject) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "sendFeedback",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {

		jstring email = methodInfo.env->NewStringUTF(reciever_emailid.c_str());
		jstring sub = methodInfo.env->NewStringUTF(subject.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, email, sub);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
//		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

bool GameUtils::isInternetAvail() {
	cocos2d::JniMethodInfo methodInfo;
	jboolean isAvail = false;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "isInternet", "()Z")) {

		isAvail = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	return isAvail;
}

void GameUtils::openPlayStore(string pubNmae) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openPlayStore",
			"(Ljava/lang/String;)V")) {
		jstring pub = methodInfo.env->NewStringUTF(pubNmae.c_str());
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, pub);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void GameUtils::rateUs(string pkg) {
	cocos2d::JniMethodInfo methodInfo;
		if (JniHelper::getStaticMethodInfo(methodInfo,
				"org/cocos2dx/cpp/AppActivity", "rateUs",
				"(Ljava/lang/String;)V")) {
			jstring pub = methodInfo.env->NewStringUTF(pkg.c_str());
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, pub);
			methodInfo.env->DeleteLocalRef(methodInfo.classID);
		}
}
