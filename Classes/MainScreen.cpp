/*
 * MainScreen.cpp
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#include "MainScreen.h"
#include "GameUtils.h"
#include "GamePlayScene.h"
#include "SonarFrameworks.h"
#include "AboutUs.h"

using namespace SonarCocosHelper;

const char* MAIN_SCREEN = "MainScreen";
MainScreen::MainScreen() {

}

MainScreen::~MainScreen() {
}

Scene* MainScreen::createScene() {
	auto scene = Scene::create();
	auto layer = MainScreen::create();
	scene->addChild(layer);
	return scene;
}

bool MainScreen::init() {
	size = Director::getInstance()->getWinSize();
//	Sprite *bg_sp = Sprite::create("bg.jpg");
//	bg_sp->setPosition(size.width / 2, size.height / 2);
//	addChild(bg_sp);

	Sprite *name = Sprite::create("tic.jpg");
	name->setPosition(size.width / 2, size.height * 0.9);
	addChild(name);

	Label* single_label = Label::createWithTTF("Single Player",
			"fonts/Marker Felt.ttf", 40);
	Label* double_label = Label::createWithTTF("Two Player",
			"fonts/Marker Felt.ttf", 40);

	MenuItem *single_player = MenuItemImage::create("button.png", "button.png",
			CC_CALLBACK_1(MainScreen::onButtonClick, this));
	single_label->setPosition(single_player->getBoundingBox().size.width / 2,
			single_player->getBoundingBox().size.height / 2);
	single_player->setTag(1);
	single_player->addChild(single_label);

	MenuItem *double_player = MenuItemImage::create("button.png", "button.png",
			CC_CALLBACK_1(MainScreen::onButtonClick, this));
	double_player->setTag(2);
	double_label->setPosition(double_player->getBoundingBox().size.width / 2,
			double_player->getBoundingBox().size.height / 2);
	double_player->addChild(double_label);

	Menu *menu = Menu::create(double_player, single_player, NULL);
	menu->setPosition(size.width / 2, size.height / 2);
	menu->alignItemsVerticallyWithPadding(15);
	addChild(menu);

	//Audio menu
	if (GameUtils::getGameUtilsInstance()->isEffectOn()) {
		sound = Sprite::create("sound_on.png");
		sound->setTag(1);
	} else {
		sound = Sprite::create("sound_off.png");
		sound->setTag(2);
	}
	float width = sound->getBoundingBox().size.width;
	float height = sound->getBoundingBox().size.height;
	sound->setPosition(Vec2(size.width - width / 1.7, size.height * 0.30));
	this->addChild(sound);

	if (GameUtils::getGameUtilsInstance()->isMusicOn()) {
		GameUtils::getGameUtilsInstance()->stopMusic();
	}

	if (GameUtils::getGameUtilsInstance()->isMusicOn()) {
		music = Sprite::create("music_on.png");
		music->setTag(3);
	} else {
		music = Sprite::create("music_off.png");
		music->setTag(4);
	}
	music->setPosition(Vec2(width / 1.7, size.height * 0.30));
	this->addChild(music);

	aboutUs = Sprite::create("info.png");
	aboutUs->setPosition(Vec2(size.width - width / 1.7, size.height * 0.70));
	aboutUs->setTag(5);
	addChild(aboutUs);
	moreApps = Sprite::create("more_app.png");
	moreApps->setTag(6);
	moreApps->setPosition(Vec2(width / 1.7, size.height * 0.70));
	addChild(moreApps);
	onClick();

	// Back Key code
	auto listener = EventListenerKeyboard::create();
	listener->onKeyReleased = [](EventKeyboard::KeyCode keyCode, Event* event) {
		SonarCocosHelper::AdMob::showFullscreenAd();
		Director::getInstance()->end();
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void MainScreen::onClick() {
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan =
			[&](cocos2d::Touch* touch, cocos2d::Event* event)
			{
				//log("On Click");
				Vec2 p = touch->getLocation();
				Rect rect = static_cast<Sprite*>(event->getCurrentTarget())->getBoundingBox();

				if(rect.containsPoint(p))
				{
					return true;
				}

				return false;
			};
	listener->onTouchEnded =
			[&](cocos2d::Touch* touch, cocos2d::Event* event)
			{
				//log("On touch listener");
				Vec2 p =touch->getLocation();
				Sprite* target = static_cast<Sprite*>(event->getCurrentTarget());
				int tag = target->getTag();

				cocos2d::TextureCache* textureCache = Director::getInstance()->getTextureCache();
				switch (tag) {
					case 1:
					sound->setTexture(textureCache->addImage("sound_off.png"));
					sound->setTag(2);
					GameUtils::getGameUtilsInstance()->setEffect(false);
					break;
					case 2:
					sound->setTexture(textureCache->addImage("sound_on.png"));
					sound->setTag(1);
					GameUtils::getGameUtilsInstance()->setEffect(true);
					break;
					case 3:
					if (GameUtils::getGameUtilsInstance()->isMusicOn()) {
						GameUtils::getGameUtilsInstance()->stopMusic();
					}
					music->setTexture(textureCache->addImage("music_off.png"));
					GameUtils::getGameUtilsInstance()->setMusic(false);
					music->setTag(4);
					break;
					case 4:
					music->setTexture(textureCache->addImage("music_on.png"));
					music->setTag(3);
					GameUtils::getGameUtilsInstance()->setMusic(true);
					break;
					case 5:
					GoogleAnalytics::sendEvent("About us", "Click", "About us", 3);
					Director::getInstance()->pushScene(AboutUs::createScene());
					//TODO about us
					break;
					case 6:
					GoogleAnalytics::sendEvent("More Apps", "Click", "MainScreen", 3);
					GameUtils::getGameUtilsInstance()->openPlayStore("AlienApps");
					break;
				}
			};

	//log("On Click_1");
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
			listener, sound);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
			listener->clone(), music);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
			listener->clone(), moreApps);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
			listener->clone(), aboutUs);
	//log("On Click_2");
}

void MainScreen::onButtonClick(Ref* sender) {
	int tag = ((Menu*) sender)->getTag();
	switch (tag) {
	case 2:
		GoogleAnalytics::sendEvent("MainScreen", "Click", "Double Player", 1);
		GameUtils::getGameUtilsInstance()->setGameType(Game_Type::Double);
		Director::getInstance()->replaceScene(GamePlayScene::createScene());
		break;
	case 1:
		GoogleAnalytics::sendEvent("MainScreen", "Click", "Single Player", 1);
		GameUtils::getGameUtilsInstance()->setGameType(Game_Type::Single);
		Director::getInstance()->replaceScene(GamePlayScene::createScene());
		break;
	}
}

