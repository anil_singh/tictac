#ifndef __ABOUTUS_SCENE_H__
#define __ABOUTUS_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;
using namespace cocos2d;


class AboutUs : public Layer
{
public:
    static Scene* createScene();
    Node* _rootNode;
    virtual bool init();
    void buttonsClicked(Ref*pSender,TouchEventType type);
    Text* gameName,*gameVersion;
    CREATE_FUNC(AboutUs);
};

#endif // __ABOUTUS_SCENE_H__
