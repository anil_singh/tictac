#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Board.h"

using namespace std;
USING_NS_CC;

class GamePlayScene: public cocos2d::Layer {
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(GamePlayScene);
	void play(LocationNode* sp);
	Size size;
	virtual void onExit();
private:
	Board *gameBoard;
	Label* label;
	bool game_over;
	void menuCloseCallback(cocos2d::Ref* pSender);
	void computerMove(float dt);
	bool checkType_1(vector<Vec2> moves, Vec2 pos, bool isx_increment);
	bool checkForWinner(vector<Vec2> moves);
	bool checkType_2(vector<Vec2> moves, Vec2 pos, bool isx_increment);
	void gameOver();



};

#endif // __HELLOWORLD_SCENE_H__
