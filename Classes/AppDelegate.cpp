#include "AppDelegate.h"
#include "MainScreen.h"
#include "SimpleAudioEngine.h"
#include "extensions/cocos-ext.h"
#include "SonarFrameworks.h"

USING_NS_CC;
USING_NS_CC_EXT;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() {
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs() {
	//set OpenGL context attributions,now can only set six attributions:
	//red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

	GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
		glview = GLViewImpl::create("My Game");
		director->setOpenGLView(glview);
	}

	// turn on display FPS
	director->setDisplayStats(false);
	glview->setDesignResolutionSize(720, 1280, ResolutionPolicy::SHOW_ALL);
	director->setViewport();
	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music_1.mp3");
	// create a scene. it's an autorelease object
	auto scene = MainScreen::createScene();

	// run
	director->runWithScene(scene);
//	SonarCocosHelper::AdMob::preLoadFullscreenAd();
	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();
	CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();

	// if you use SimpleAudioEngine, it must resume here
	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
