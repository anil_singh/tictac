/*
 * LocationNode.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#include "LocationNode.h"
#include "Board.h"

LocationNode::LocationNode() {
	no_Characte = 0;
	isOcupied = false;
	charater = Charater_Type::Blank;
	size = Director::getInstance()->getWinSize();
}

LocationNode::~LocationNode() {
}

LocationNode* LocationNode::create() {
	LocationNode* location = new LocationNode();

	if (location && location->initWithFile("none.jpg")) {
		location->autorelease();

		location->addEvents();
//		location->setLabel();
		return location;
	}

	CC_SAFE_DELETE(location);
	return NULL;
}

void LocationNode::setnod_Cordinate(Vec2 postion) {
	this->position = postion;
}

Vec2 LocationNode::getnod_Cordinate() {
	return position;
}

void LocationNode::setOcupied(bool ocupied) {
	this->isOcupied = ocupied;
}

bool LocationNode::is_Ocupied() {
	return isOcupied;
}

void LocationNode::set_Type(Charater_Type type) {
	this->charater = type;
}

Charater_Type LocationNode::get_Type() {
	return charater;
}

void LocationNode::setno_Character(int no_charater) {
	this->no_Characte = no_charater;
//	updateLabel();
}

int LocationNode::getno_Character() {
	return no_Characte;
}

void LocationNode::addEvents() {
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		Vec2 p = this->getParent()->convertToNodeSpace(touch->getLocation());
		Rect rect = this->getBoundingBox();

		if(rect.containsPoint(p))
		{
			GameUtils::getGameUtilsInstance()->vibrateIt(50L);
			return true;
		}

		return false;
	};

	listener->onTouchEnded = [&](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		GameUtils::getGameUtilsInstance()->stopVibration();
		((Board*)this->getParent())->onGame_board_CallBack(this);
//		((Board*)this->getParent())->onGame_board_CallBack(this);
	};

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
			listener, this);
}

void LocationNode::setLabel() {
	label = LabelTTF::create(Value(no_Characte).asString(), "fonts\arial.ttf",
			35);
	label->retain();
	label->setColor(Color3B::BLUE);
	Size size = this->getBoundingBox().size;
	label->setPosition(size.width / 2, size.height / 2);
	this->addChild(label);
}

void LocationNode::updateLabel() {
//	log("In the Location updateLabel %d", no_Characte);
	label->setString(Value(this->getTag()).asString());
}
