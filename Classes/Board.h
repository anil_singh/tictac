/*
 * Board.h
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "cocos2d.h"
#include "LocationNode.h"
USING_NS_CC;

class Board : public Layer
{

private:
	int no_grid;
	LocationNode* loc_node;
	Size size;
	int stepX, stepY;
	Vec2 setLocNodePos(int x, int y, int y_chang, LocationNode* loc_1);
public:
	Vector<LocationNode*> locNodeList;
	int move_count;
	Board();
	virtual ~Board();
	void onGame_board_CallBack(LocationNode *loc_sp);

	void setno_Grid(int no_grid);
	int getno_Grid();
	void setNodeChild(LocationNode* loc_node);
	void drawCrossLine(Vec2 startPoint, Vec2 endPoint);


	void createGrid();

	CREATE_FUNC(Board);
};

#endif /* BOARD_H_ */
