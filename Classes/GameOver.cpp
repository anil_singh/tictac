/*
 * GameOver.cpp
 *
 *  Created on: Nov 20, 2015
 *      Author: anil
 */

#include "GameOver.h"
#include "MainScreen.h"
#include "GameUtils.h"
#include "GamePlayScene.h"
#include "SonarFrameworks.h"
using namespace SonarCocosHelper;

string GameOver::name;
const char* GAME_OVER = "Game Over";
Scene* GameOver::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameOver::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GameOver::init() {
	size = Director::getInstance()->getWinSize();
//	Sprite *bg_sp = Sprite::create("bg.jpg");
//	bg_sp->setPosition(size.width / 2, size.height / 2);
//	addChild(bg_sp);

	Label* winner = Label::createWithTTF(name + " won this match",
			"fonts/Marker Felt.ttf", 45);
	winner->setPosition(Vec2(size.width / 2, size.height * .25));
	winner->setColor(Color3B::WHITE);
	addChild(winner);

	Label* single_label = Label::createWithTTF("Restart",
			"fonts/Marker Felt.ttf", 40);
	Label* double_label = Label::createWithTTF("Main Menu",
			"fonts/Marker Felt.ttf", 40);

	MenuItem *single_player = MenuItemImage::create("button.png", "button.png",
			CC_CALLBACK_1(GameOver::onButtonClick, this));
	single_label->setPosition(single_player->getBoundingBox().size.width / 2,
			single_player->getBoundingBox().size.height / 2);
	single_player->setTag(2);
	single_player->addChild(single_label);

	MenuItem *double_player = MenuItemImage::create("button.png", "button.png",
			CC_CALLBACK_1(GameOver::onButtonClick, this));
	double_player->setTag(1);
	double_label->setPosition(double_player->getBoundingBox().size.width / 2,
			double_player->getBoundingBox().size.height / 2);
	double_player->addChild(double_label);

	Menu *menu = Menu::create(double_player, single_player, NULL);
	menu->setPosition(size.width / 2, size.height * .15);
	menu->alignItemsHorizontallyWithPadding(20);
	addChild(menu);

}

void GameOver::onButtonClick(Ref* sender) {
	int tag = ((Menu*) sender)->getTag();
	switch (tag) {
	case 2:
		GoogleAnalytics::sendEvent("Restart Game", "Click", "Game Over", 2);
		GameUtils::getGameUtilsInstance()->re_initialize();
		Director::getInstance()->replaceScene(GamePlayScene::createScene());
		break;
	case 1:
//		AdMob::showPreLoadedFullscreenAd();
		GoogleAnalytics::sendEvent("Main Menu", "Click", "Game Over", 2);
		GameUtils::getGameUtilsInstance()->re_initialize();
		Director::getInstance()->replaceScene(MainScreen::createScene());
		break;
	}
}

GameOver::GameOver() {
	// TODO Auto-generated constructor stub

}

GameOver::~GameOver() {
}

