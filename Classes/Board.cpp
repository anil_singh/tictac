/*
 * Board.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#include "Board.h"
#include "GamePlayScene.h"

Board::Board() {
	stepX = stepY = 10;
	size = Director::getInstance()->getWinSize();
	move_count = 0;
	loc_node = nullptr;
	no_grid = 3;
	stepX = 240;
	stepY = 240;
	createGrid();
}

Board::~Board() {
}

int Board::getno_Grid() {
	return no_grid;
}

void Board::createGrid() {
	float y_change = 0;
	float x_change = 0;
	for (int i = 0; i < no_grid; ++i) {
		y_change = (size.height * .10) + (i * stepY);

		for (int j = 0; j < no_grid; ++j) {
			x_change = (size.width * .10) + (j * stepX);
			LocationNode* tem_loc = LocationNode::create();
			tem_loc->setnod_Cordinate(Vec2(i, j));
			tem_loc->setOcupied(false);
			tem_loc->setno_Character(0);
			tem_loc->set_Type(Charater_Type::Blank);
			tem_loc->setTag((no_grid * i) + j);
//			tem_loc->updateLabel();
			/*log("In the Board create x = %f, y = %f",
					tem_loc->getnod_Cordinate().x,
					tem_loc->getnod_Cordinate().y);*/
			tem_loc->setPosition(x_change, y_change);

			this->addChild(tem_loc);
		}
	}
}

Vec2 Board::setLocNodePos(int x, int y, int y_chang, LocationNode* loc_1) {
	Vec2 pos;
	pos.x = loc_1->getPositionX() + loc_1->getBoundingBox().size.width
			+ (x * stepX);
	pos.y = y_chang;	//+ (loc_1->getBoundingBox().size.width );
	return pos;
}

void Board::onGame_board_CallBack(LocationNode *pos_sp) {
	GameUtils * gameUtils = GameUtils::getGameUtilsInstance();
	if (gameUtils->getCurrentTurn() != Turn::None && !pos_sp->is_Ocupied()
			&& !(gameUtils->getGameType() == Game_Type::Single
					&& gameUtils->getCurrentTurn() == Turn::PlayerB)) {
//		log("In game_board_callback");
		((GamePlayScene*) this->getParent())->play(pos_sp);
	}
}

void Board::drawCrossLine(Vec2 startPoint, Vec2 endPoint) {
//	log("Draw line is called");
	DrawNode *drawNode = DrawNode::create();
	this->addChild(drawNode, 2);
	LocationNode* start_node = (LocationNode*) this->getChildByTag(
			startPoint.x * 3 + startPoint.y);
	LocationNode* end_node = (LocationNode*) this->getChildByTag(
			endPoint.x * 3 + endPoint.y);
	drawNode->drawSegment(start_node->getPosition(), end_node->getPosition(), 5,
			Color4F::RED);
}
