/*
 * GameOver.h
 *
 *  Created on: Nov 20, 2015
 *      Author: anil
 */

#ifndef GAMEOVER_H_
#define GAMEOVER_H_

#include "cocos2d.h"

using namespace std;

USING_NS_CC;
class GameOver : public Layer{
public:
public:
	GameOver();
	virtual ~GameOver();
	bool init();
	static Scene* createScene();
	static string name;
	CREATE_FUNC(GameOver);

private:
	Size size;


	void onButtonClick(Ref* sender);
};

#endif /* GAMEOVER_H_ */
