/*
 * TictacAI.cpp
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#include "TictacAI.h"

TictacAI::TictacAI() {
}

TictacAI::~TictacAI() {
}

bool TictacAI::alternateMove(Board* gameBoard, vector<Vec2> moves) {
	GameUtils *gameUtils = GameUtils::getGameUtilsInstance();
	vector<int> corner_mov = gameUtils->getCornerMoves();
	vector<int> side_mov = gameUtils->getSideMoves();
//	log("TictacAI Last chance start size_corner = %d  size_side = %d",
//			corner_mov.size(), corner_mov.size());
	bool result = false;

	int tag = 0;
	if (gameUtils->getTotalMoves() < 2) {

//		log("TictacAI blockMove 1 ");
		LocationNode* temp = ((LocationNode*) gameBoard->getChildByTag(4));
		if (!temp->is_Ocupied()) {
			changeTurn(temp);
			return true;
		} else {
			int ran = RandomHelper::random_int(0, 3);

			temp =
					((LocationNode*) gameBoard->getChildByTag(
							corner_mov.at(ran)));
			changeTurn(temp);
			return true;
		}
	}

//	log("TictacAI Last chance middle");

	int count = 0;
	while (count < corner_mov.size() / 2) {
		//log("TictacAI blockMove 2 ");
		LocationNode* temp = ((LocationNode*) gameBoard->getChildByTag(count));
		if (!temp->is_Ocupied()) {
			temp = ((LocationNode*) gameBoard->getChildByTag(count + 6));
			LocationNode* temp2 = ((LocationNode*) gameBoard->getChildByTag(4));
			if (!temp2->is_Ocupied() && temp->is_Ocupied()) {
				temp = ((LocationNode*) gameBoard->getChildByTag(count));
				changeTurn(temp);
				//log("TictacAI return from changeTurn");
				return true;
			}
			count++;
		} else {
			count++;
		}
	}
	//log("TictacAI Last chance second middle");
	count = 0;
	while (count < side_mov.size() / 2) {
		//log("TictacAI blockMove 3 ");
		LocationNode* temp = ((LocationNode*) gameBoard->getChildByTag(count));
		if (!temp->is_Ocupied()) {
			temp = ((LocationNode*) gameBoard->getChildByTag(count + 4));
			LocationNode* temp2 = ((LocationNode*) gameBoard->getChildByTag(4));
			//log("TictacAI blockMove 3 temp2 %d", temp2->is_Ocupied());
			if (!temp2->is_Ocupied() && temp->is_Ocupied()) {
				temp = ((LocationNode*) gameBoard->getChildByTag(count));
				changeTurn(temp);
				return true;
			}
			count++;
		} else {
			count++;
		}
	}

	if (!result) {
		//log("TictacAI blockMove 4 ");
		while (!result) {
			int var = RandomHelper::random_int(0, 7);
			LocationNode *temp = ((LocationNode*) gameBoard->getChildByTag(var));
			if (!temp->is_Ocupied()) {
				changeTurn(temp);
				result = true;
				break;
			}
		}
	}
	//log("TictacAI Last chance finish");

	return result;
}

void TictacAI::changeTurn(LocationNode * temp) {
//	log("TictacAI changeTurn 1");
	GameUtils *gameUtils = GameUtils::getGameUtilsInstance();
	temp->setTexture(
			Director::getInstance()->getTextureCache()->addImage("zero.png"));
	temp->set_Type(Charater_Type::Zero);
	temp->setOcupied(true);
	gameUtils->setCurrentTurn(Turn::PlayerA);
	gameUtils->setPlayerB_Moves(1);
	gameUtils->setListPB_MovesCoor(temp->getnod_Cordinate());
}

bool TictacAI::AllCheck(Board* gameBoard, Charater_Type type,
		vector<Vec2> moves) {
	bool result = false;
	// Horizontal

	if (checkType_1(gameBoard, type, moves, Vec2(0, 0), true)) {
		log("In the 0 0 vertical");
		result = true;
	} else if (checkType_1(gameBoard, type, moves, Vec2(0, 1), true)) {
		log("In the 0 1 vertical");
		result = true;
	} else if (checkType_1(gameBoard, type, moves, Vec2(0, 2), true)) {
		log("In the 0 2 vertical");
		result = true;
	} else if (checkType_2(gameBoard, type, moves, Vec2(0, 0), false)) {
		log("In the 0 0 diagonal");
		result = true;
	} else if (checkType_2(gameBoard, type, moves, Vec2(2, 0), true)) {
		log("In the 2 0 diagonal");
		result = true;
	} else if (checkType_1(gameBoard, type, moves, Vec2(0, 0), false)) {
		log("In the 0 0 horizontal");
		result = true;
	} else if (checkType_1(gameBoard, type, moves, Vec2(1, 0), false)) {
		log("In the 1 0 horizontal");
		result = true;
	} else if (checkType_1(gameBoard, type, moves, Vec2(2, 0), false)) {
		log("In the 2 0 horizontal");
		result = true;
	}
	log("In the result = %d", result);
	return result;
}

bool TictacAI::checkType_2(Board* gameBoard, Charater_Type type,
		vector<Vec2> moves, Vec2 pos, bool alternate) {
	bool result = false;
	int count = 0;
	int success_count = 0;
	int tag = 0;
	LocationNode *empty_locNode_Sp;
	bool empty_block = false;
	Vec2 tempPos;

//	log("In the Check2 posx = %f   posy = %f", pos.x, pos.y);

	while (alternate ?
			((pos.x - count) > -1 && (pos.y + count) < 3) :
			((pos.x + count) < 3 && (pos.y + count) < 3)) {
		tempPos =
				alternate ?
						Vec2((pos.x - count), (pos.y + count)) :
						Vec2((pos.x + count), (pos.y + count));

//		log("Pos-x = %f  Pos-y = %f", tempPos.x, tempPos.y);

		if (GameUtils::getGameUtilsInstance()->customFind(moves, tempPos)) {
			success_count++;
			count++;
		} else {
			tag = tempPos.x * 3 + tempPos.y;
			LocationNode* tempSp = (LocationNode*) gameBoard->getChildByTag(
					tag);
			if (tempSp->is_Ocupied()) {
				break;
			} else {
				empty_block = true;
				empty_locNode_Sp = tempSp;
				count++;
			}
		}
	}

	if (success_count > 1 && empty_block) {
		changeTurn(empty_locNode_Sp);
		result = true;
	}
	return result;
}

bool TictacAI::checkType_1(Board* gameBoard, Charater_Type type,
		vector<Vec2> moves, Vec2 pos, bool isx_increment) {
	bool result = false;
	int count = 0;
	int success_count = 0;
	int tag = 0;
	LocationNode *empty_locNode_Sp;
	Vec2 tempPos;
	bool empty_block = false;
	int temp = isx_increment ? pos.x : pos.y;

//	log("In the Check1 posx = %f   posy = %f", pos.x, pos.y);

	while ((temp + count) < 3) {
		tempPos =
				isx_increment ?
						Vec2((pos.x + count), pos.y) :
						Vec2(pos.x, (pos.y + count));

//		log("Pos-x = %f  Pos-y = %f", tempPos.x, tempPos.y);

		if (GameUtils::getGameUtilsInstance()->customFind(moves, tempPos)) {
			success_count++;
			count++;
		} else {
			tag = tempPos.x * 3 + tempPos.y;
			LocationNode* tempSp = (LocationNode*) gameBoard->getChildByTag(
					tag);
			if (tempSp->is_Ocupied()) {
				break;
			} else {
				empty_block = true;
				empty_locNode_Sp = tempSp;
				count++;
			}
		}
	}

	if (success_count > 1 && empty_block) {
		changeTurn(empty_locNode_Sp);
		result = true;
	}
	return result;
}
