#include "AboutUs.h"
#include "cocostudio/ActionTimeline/CSLoader.h"
#include "GameUtils.h"
#include "SonarFrameworks.h"



USING_NS_CC;

Scene* AboutUs::createScene()
{
    auto scene = Scene::create();
    auto layer = AboutUs::create();
    scene->addChild(layer);
    
    return scene;
}

bool AboutUs::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    Size size = Director::getInstance()->getWinSize();
    _rootNode=CSLoader::createNode("AboutUs/MainScene.csb");
    this->addChild(_rootNode);
    
   Vector<Node*> child =  _rootNode->getChildren();
   for (int var = 0; var < child.size(); ++var) {
	   Node* node = child.at(var);
	log("id = %d", node->getTag());
}

    Button*fbLike=static_cast<Button*>(_rootNode->getChildByTag(0));
    fbLike->addTouchEventListener(this,toucheventselector(AboutUs::buttonsClicked));

    Button*twitterFollow=static_cast<Button*>(_rootNode->getChildByTag(1));
    twitterFollow->addTouchEventListener(this,toucheventselector(AboutUs::buttonsClicked));
    
    Button*rateUs=static_cast<Button*>(_rootNode->getChildByTag(2));
    rateUs->addTouchEventListener(this,toucheventselector(AboutUs::buttonsClicked));
    
    Button*feedBaack=static_cast<Button*>(_rootNode->getChildByTag(3));
    feedBaack->addTouchEventListener(this,toucheventselector(AboutUs::buttonsClicked));
    
    Sprite*background=static_cast<Sprite*>(_rootNode->getChildByTag(6));
    
    gameName=static_cast<Text*>(_rootNode->getChildByTag(4));
    gameName->setVisible(false);
    
    gameVersion=static_cast<Text*>(_rootNode->getChildByTag(5));
    gameVersion->setPosition(Vec2(size.width/2, size.height *.78));
    gameVersion->setBrightStyle(BrightStyle::HIGHLIGHT);
    gameVersion->setTextColor(Color4B::BLUE);
    gameVersion->setFontSize(50);
    
    auto listener = EventListenerKeyboard::create();
    	listener->onKeyReleased = [](EventKeyboard::KeyCode keyCode, Event* event) {
    		Director::getInstance()->popScene();
    	};

    	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void AboutUs::buttonsClicked(Ref*pSender,TouchEventType type)
{
	GameUtils *gameUtils = GameUtils::getGameUtilsInstance();
    int tag = ((Button*) pSender)->getTag();
    switch (type)
    {
        case TouchEventType::TOUCH_EVENT_BEGAN:
        {
            if(tag==0)
            {
                //fb like clicked
//                log("button clicked facebook like");
                SonarCocosHelper::GoogleAnalytics::sendEvent("Facebook page", "Click", "AboutUs",1);
                gameUtils->openUrl("fb://page/771855222903616", "https://www.facebook.com/771855222903616");
            }
            else if (tag==1)
            {
                //twitter button clicked
//                log("button clicked twitter");
                SonarCocosHelper::GoogleAnalytics::sendEvent("Twitter", "Click", "AboutUs", 2);
                gameUtils->openUrl("twitter://user?screen_name=AAD_APPS", "https://twitter.com/AAD_APPS");
            }
            else if (tag==2)
            {
                //rate us clicked
//                log("button clicked rate us");
                SonarCocosHelper::GoogleAnalytics::sendEvent("Rate us", "Click", "AboutUs",3);
                gameUtils->rateUs("com.aks.ttg");
            }
            else if (tag==3)
            {
                //feedback clicked
//                log("button clicked feedback");
                SonarCocosHelper::GoogleAnalytics::sendEvent("Feed back", "Click", "AboutUs",4);
                std::string email = "alienappsdeveloper@gmail.com" ;
                std::string sub = "Feedback for tictactoe";
                gameUtils->sendFeedback(email, sub);
            }
        }
            break;
    }
}


