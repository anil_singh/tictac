/*
 * LocationNode.h
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#ifndef LOCATIONNODE_H_
#define LOCATIONNODE_H_

#include "cocos2d.h"
#include "GameUtils.h"
USING_NS_CC;

class LocationNode : public Sprite
{
private:

	Size size;
	Vec2 position;
	bool isOcupied;
	int no_Characte;
	Charater_Type charater;
	LabelTTF* label;
	void setLabel();


public:
	void updateLabel();
	LocationNode();
	virtual ~LocationNode();
	static LocationNode* create();
	void setnod_Cordinate(Vec2 postion);
	Vec2 getnod_Cordinate();
	bool is_Ocupied();
	void setOcupied(bool occupied);
	void setno_Character(int no_charater);
	int getno_Character();
	void set_Type(Charater_Type type);
	Charater_Type get_Type();
	void addEvents();

};

#endif /* LOCATIONNODE_H_ */
