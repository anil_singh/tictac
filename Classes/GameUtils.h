/*
 * GameUtils.h
 *
 *  Created on: Nov 15, 2015
 *      Author: anil
 */

#ifndef GAMEUTILS_H_
#define GAMEUTILS_H_

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

enum Turn {
	None, PlayerA, PlayerB, Computer
};
enum Charater_Type {
	Blank, Cross, Zero
};
enum Game_Type {
	Single, Double
};
class GameUtils {
public:
	GameUtils();
	virtual ~GameUtils();
	static GameUtils* getGameUtilsInstance();
	Turn getCurrentTurn();
	void setCurrentTurn(Turn turn);
	void setGameType(Game_Type game_type);
	Game_Type getGameType();

	void vibrateIt(long time);
	void stopVibration();

	void setPlayerA_Moves(int count);
	void setPlayerB_Moves(int count);
	int getPlayerA_Moves();
	int getPlayerB_Moves();
	int getTotalMoves();
	Turn getFirstTurn();
	void setFirstTurn(Turn fst_turn);
	void setListPA_MovesCoor(Vec2 pos);
	void setListPB_MovesCoor(Vec2 pos);
	vector<Vec2> getListPA_MovesCoor();
	vector<Vec2> getListPB_MovesCoor();
	vector<int> getCornerMoves();
	vector<int> getSideMoves();
	string getPlayerName(Turn type);
	bool customFind(vector<Vec2> moves, Vec2 toFind);
	bool isEffectOn();
	void setEffect(bool effect);
	bool isMusicOn();
	void setMusic(bool music);
	void re_initialize();
	void playEffect();
	void playMusic();
	void stopMusic();
	void openUrl(string url, string altrUrl);
	void sendFeedback(string reciever_emailid, string subject);
	bool isInternetAvail();
	void openPlayStore(string pubName);
	void rateUs(string pkg);

private:
	static GameUtils* gameUtilsInstance;
	int move_count, playerA_count, playerB_count;
	vector<Vec2> playerA_Moves, playerB_Moves;
	Turn turn, first_turn;
	Game_Type game_type;
	vector<int> cornerMoves, sideMoves;
};

#endif /* GAMEUTILS_H_ */
