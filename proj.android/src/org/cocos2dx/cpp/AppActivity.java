/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.cpp;

import java.util.regex.Pattern;

import sonar.systems.framework.SonarFrameworkActivity;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;
import android.util.Patterns;

public class AppActivity extends SonarFrameworkActivity {

	public static void vibrateIt(long time) {
		Vibrator v = (Vibrator) AppActivity.getContext().getSystemService(
				Context.VIBRATOR_SERVICE);
		v.vibrate(time);
	}
	
	public static void stopVibration() {
		Vibrator v = (Vibrator) AppActivity.getContext().getSystemService(
				Context.VIBRATOR_SERVICE);
		v.cancel();
	}

	public static void openUrl(String url, String altrUrl) {
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
			AppActivity.getContext().startActivity(intent);
		} catch (Exception e) {
			 Intent intent =  new Intent(Intent.ACTION_VIEW, Uri.parse(altrUrl));
			 AppActivity.getContext().startActivity(intent);
		}
	}

	public static void sendFeedback(String reciever_emailid, String subject) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("text/plain");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { reciever_emailid });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);

		try {
			AppActivity.getContext()
					.startActivity(
							Intent.createChooser(emailIntent,
									"Send Feedback using..."));
		} catch (android.content.ActivityNotFoundException ex) {

		}
	}

	private static String registerEmailid() {
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		Account[] accounts = AccountManager.get(AppActivity.getContext())
				.getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				possibleEmail = account.name;
			}
		}
		return possibleEmail;
	}

	public static boolean isInternet() {
		ConnectivityManager conMgr = (ConnectivityManager) AppActivity
				.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			// notify user you are online
			return true;
		} else {
			// notify user you are not online
			return false;
		}
	}

	public static void openPlayStore(String pubName) {
		try {
			AppActivity.getContext().startActivity(
					new Intent(Intent.ACTION_VIEW, Uri
							.parse("market://search?q=pub:" + pubName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			AppActivity.getContext().startActivity(
					new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://play.google.com/store/search?q=pub:"
									+ pubName)));
		}
	}
	
	public static void rateUs(String pakageName) {
		Log.d("", "Pakage name " + pakageName);
		try {
			AppActivity.getContext().startActivity(
					new Intent(Intent.ACTION_VIEW, Uri
							.parse("market://details?id=" + pakageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			AppActivity.getContext().startActivity(
					new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://play.google.com/store/apps/details?id="
									+ pakageName)));
		}
	}
}
